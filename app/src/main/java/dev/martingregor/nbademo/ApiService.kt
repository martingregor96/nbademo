package dev.martingregor.nbademo

import retrofit2.http.GET
import retrofit2.http.Query

interface NBAApiService {
    @GET("/api/v1/players")
    suspend fun getPlayers(
        @Query("page") page: Int = 0,
        @Query("per_page") count: Int = 25
    ): PlayersDto
}