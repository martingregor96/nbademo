package dev.martingregor.nbademo

import com.squareup.moshi.JsonClass
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

private const val BASE_URL = "https://www.balldontlie.io"

private val okHttpClient: OkHttpClient = OkHttpClient.Builder()
    .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
    .build()

private val retrofit: Retrofit = Retrofit.Builder()
    .baseUrl(BASE_URL)
    .client(okHttpClient)
    .addConverterFactory(MoshiConverterFactory.create())
    .build()

object NBAApi {
    val retrofitService: NBAApiService by lazy {
        retrofit.create(NBAApiService::class.java)
    }
}

@JsonClass(generateAdapter = true)
data class TeamDto(
    val id: Long,
    val abbreviation: String,
    val city: String,
    val conference: String,
    val division: String,
    val full_name: String,
    val name: String
)

@JsonClass(generateAdapter = true)
data class PlayerDto(
    val id: Long,
    val first_name: String,
    val last_name: String,
    val position: String,
    val height_feet: Int?,
    val height_inches: Int?,
    val weight_pounds: Int?,
    val team: TeamDto,
    var expanded: Boolean = false // PlayerDto should be mapped to domain object first
)

@JsonClass(generateAdapter = true)
data class MetaInfoDto(
    val total_pages: Int,
    // ---- ----
)

@JsonClass(generateAdapter = true)
data class PlayersDto(
    val data: List<PlayerDto>,
    val meta: MetaInfoDto
)