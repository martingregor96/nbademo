package dev.martingregor.nbademo.helpers

import android.util.Log
import kotlinx.coroutines.delay
import java.io.IOException
import java.net.SocketTimeoutException
import java.net.UnknownHostException

suspend fun <T> safeApiCall(apiCall: suspend () -> T): T {
    while (true) {
        try {
            return apiCall.invoke()
        } catch (exception: Exception) {
            when (exception) {
                is UnknownHostException -> Log.e("NetworkCallFailed", "UnknownHostException")
                is SocketTimeoutException -> Log.e("NetworkCallFailed", "SocketTimeoutException")
                is IOException -> Log.e("NetworkCallFailed", "IOException")
                else -> Log.e("NetworkCallFailed", "Exception - $exception")
            }
            delay(2000)
        }
    }
}
