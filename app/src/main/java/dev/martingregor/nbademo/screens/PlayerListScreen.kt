package dev.martingregor.nbademo.screens

import androidx.compose.animation.AnimatedContent
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.animation.animateColorAsState
import androidx.compose.animation.core.animateDpAsState
import androidx.compose.animation.core.tween
import androidx.compose.animation.expandVertically
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.animation.shrinkVertically
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.pullrefresh.PullRefreshIndicator
import androidx.compose.material.pullrefresh.pullRefresh
import androidx.compose.material.pullrefresh.rememberPullRefreshState
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.ElevatedCard
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.LocalContentColor
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import dev.martingregor.nbademo.PlayerDto

@OptIn(ExperimentalAnimationApi::class, ExperimentalMaterialApi::class)
@Composable
fun PlayerListScreen(viewModel: PlayerListViewModel = viewModel()) {
    val state = viewModel.state
    val players = state.players

    AnimatedContent(state.loading) { loading ->
        when (loading) {
            true -> {
                Box(
                    Modifier.fillMaxSize(), contentAlignment = Alignment.Center
                ) {
                    CircularProgressIndicator()
                }
            }

            false -> {
                val listState = rememberLazyListState()
                val loadMore by remember {
                    derivedStateOf {
                        state.canLoadMore && (listState.layoutInfo.visibleItemsInfo.lastOrNull()?.index ?: -2) >= listState.layoutInfo.totalItemsCount - 1
                    }
                }
                LaunchedEffect(loadMore) {
                    if (loadMore) {
                        viewModel.loadMore()
                    }
                }
                val pullRefreshState = rememberPullRefreshState(!state.canLoadMore, {})
                Box(Modifier.pullRefresh(pullRefreshState, false)) {
                    LazyColumn(
                        Modifier.fillMaxSize(),
                        state = listState,
                        contentPadding = PaddingValues(16.dp),
                        verticalArrangement = Arrangement.spacedBy(16.dp)
                    ) {
                        itemsIndexed(players) { index, it ->
                            Player(viewModel, index, it)
                        }
                    }
                    PullRefreshIndicator(
                        !state.canLoadMore,
                        pullRefreshState,
                        Modifier.align(Alignment.TopCenter)
                    )
                }
            }
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun Player(viewModel: PlayerListViewModel, index: Int, player: PlayerDto) {
    val expanded = player.expanded
    ElevatedCard(onClick = {
        viewModel.setExpanded(index)
    }) {
        Column(
            Modifier
                .fillMaxWidth()
                .padding(top = 16.dp)
        ) {
            Text(
                "${player.first_name} ${player.last_name}",
                Modifier.padding(start = 16.dp),
                color = MaterialTheme.colorScheme.primary,
                style = MaterialTheme.typography.titleLarge
            )
            PlayerRow(
                "Position",
                player.position.ifEmpty { "Unknown" },
                startPadding = 16.dp,
                endPadding = 16.dp,
            )
            AnimatedVisibility(
                visible = expanded,
                enter = fadeIn(tween(400)) + expandVertically(tween(400)),
                exit = fadeOut(tween(400)) + shrinkVertically(tween(400)),
            ) {
                Column(Modifier.padding(horizontal = 16.dp)) {
                    PlayerRow(
                        "Height feet",
                        if (player.height_feet == null) "Unknown" else "${player.height_feet}"
                    )
                    PlayerRow(
                        "Height inches",
                        if (player.height_inches == null) "Unknown" else "${player.height_inches}"
                    )
                    PlayerRow(
                        "Weight pounds",
                        if (player.weight_pounds == null) "Unknown" else "${player.weight_pounds}"
                    )
                }
            }
            val delay = if (expanded) 400 else 0
            val topPadding by animateDpAsState(
                if (expanded) 16.dp else 4.dp, tween(400, delay), label = "startPadding"
            )
            val background by animateColorAsState(
                if (expanded) MaterialTheme.colorScheme.primaryContainer else Color.Unspecified,
                tween(400, delay),
                label = "background"
            )
            Column(
                Modifier
                    .padding(top = topPadding)
                    .background(background)
                    .padding(start = 16.dp, end = 16.dp, bottom = 16.dp)
            ) {
                PlayerRow(
                    "Team", player.team.full_name, topPadding = topPadding
                )
                AnimatedVisibility(
                    visible = expanded,
                    enter = fadeIn(tween(400, delay)) + expandVertically(
                        tween(
                            400, delay
                        )
                    ),
                    exit = fadeOut(tween(400, delay)) + shrinkVertically(
                        tween(
                            400, delay
                        )
                    ),
                ) {
                    Column {
                        PlayerRow(
                            "Name", player.team.name
                        )
                        PlayerRow(
                            "Abbreviation", player.team.abbreviation
                        )
                        PlayerRow(
                            "Division", player.team.division
                        )
                        PlayerRow(
                            "City", player.team.city
                        )
                        PlayerRow(
                            "Conference", player.team.conference
                        )
                    }
                }
            }
        }
    }
}

@Composable
fun PlayerRow(
    firstText: String,
    secondText: String,
    startPadding: Dp = 0.dp,
    topPadding: Dp = 8.dp,
    endPadding: Dp = 0.dp
) {
    Row(
        Modifier
            .padding(start = startPadding, top = topPadding, end = endPadding)
            .fillMaxWidth(),
        horizontalArrangement = Arrangement.SpaceBetween
    ) {
        Text(
            firstText,
            color = MaterialTheme.colorScheme.secondary,
            style = MaterialTheme.typography.titleMedium
        )
        Dots(Modifier.weight(1f))
        Text(
            secondText, style = MaterialTheme.typography.titleMedium
        )
    }
}

@Composable
fun Dots(modifier: Modifier = Modifier) {
    val dots =
        ". . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . ."

    Text(
        dots,
        modifier.padding(horizontal = 8.dp),
        color = LocalContentColor.current.copy(0.2f),
        softWrap = false,
        maxLines = 1,
        style = MaterialTheme.typography.titleMedium
    )
}