package dev.martingregor.nbademo.screens

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.neverEqualPolicy
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dev.martingregor.nbademo.NBAApi
import dev.martingregor.nbademo.PlayerDto
import dev.martingregor.nbademo.helpers.safeApiCall
import kotlinx.coroutines.launch

class PlayerListViewModel : ViewModel() {
    private val api = NBAApi.retrofitService

    data class PlayerListState(
        val loading: Boolean = true,
        val canLoadMore: Boolean = true,
        val players: List<PlayerDto> = emptyList()
    )

    var state by mutableStateOf(PlayerListState(), neverEqualPolicy())

    private var page = 2 // Page 0 and 1 return the same data

    init {
        viewModelScope.launch {
            val players = safeApiCall {
                api.getPlayers()
            }.data
            state = PlayerListState(
                loading = false,
                players = players
            )
        }
    }

    fun setExpanded(position: Int) {
        state.players[position].expanded = !state.players[position].expanded
        state = state // This forces recomposition because of the neverEqualPolicy() set above
    }

    fun loadMore() {
        state = state.copy(canLoadMore = false)
        viewModelScope.launch {
            val newPlayers = safeApiCall {
                api.getPlayers(
                    page = page++,
                    count = 35
                )
            }.data
            state = state.copy(
                canLoadMore = true,
                players = state.players + newPlayers
            )
        }
    }
}