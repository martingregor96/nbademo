<h1 align="center">NBADemo 🏀</h1>

## Introduction
Demo application which downloads player information from https://www.balldontlie.io and presents it to the user.

## Screenshots
<p align="center">
    <img src="media/screen0.png" width="256"/>
    <img src="media/screen1.png" width="256"/>
</p>

## Features
- [x] Written in Kotlin using MVVM architecture
- [x] Follows clean code principles
- [x] Uses Retrofit for network communication
- [x] UI written in Jetpack Compose
- [ ] No images present, therefore, no need for Glide or any other image loading library

## Warnings
Animations will suffer in speed and quality unless the app is built in release variant.

## IDE Version
Android Studio Giraffe | 2022.3.1 Canary 8